import 'package:flutter/material.dart';
import 'package:second_taxk/home/home.dart';
import 'package:second_taxk/pages/Introduction.dart';

class Splash3 extends StatefulWidget {
  static const String routeName = "/Splash3" ;

  @override
  _Splash3State createState() => _Splash3State();
}

class _Splash3State extends State<Splash3> {
  @override
  Widget build(BuildContext context) {
    final ScreenHeigh = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        width: double.infinity,
        height: ScreenHeigh,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Container(
                    width: 220,
                    height: 220,
                    decoration: BoxDecoration(image: DecorationImage(image: AssetImage("assets/images/location.png"))),
                    ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  "easy location",
                  style: TextStyle(
                    color: Colors.pink,
                    fontWeight: FontWeight.bold,
                    fontSize: 28,
                  ),
                ),
              ],
            ),
            Container(
              width: double.infinity,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed(Introduction.routeName);
                },
                child: Text("next" , style: TextStyle(color: Colors.pink , fontSize: 20 , fontWeight: FontWeight.bold , fontStyle: FontStyle.italic),),
                color: Colors.amber,
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                elevation: 0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
