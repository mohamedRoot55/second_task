import 'package:flutter/material.dart';

import 'Splash3.dart';

class Splash2 extends StatefulWidget {
  static const String routeName = "/Splash2" ;
  @override
  _Splash2State createState() => _Splash2State();
}

class _Splash2State extends State<Splash2> {
  @override
  Widget build(BuildContext context) {
    final ScreenHeigh = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        width: double.infinity,
        height: ScreenHeigh,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Container(
                    width: 220,
                    height: 220,
                    decoration: BoxDecoration(image: DecorationImage(image: AssetImage("assets/images/rocket1.png"))),
                    ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  "heigh performance",
                  style: TextStyle(
                    color: Colors.pink,
                    fontWeight: FontWeight.bold,
                    fontSize: 28,
                  ),
                ),
              ],
            ),
            Container(
              width: double.infinity,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed(Splash3.routeName);
                },
                child: Text("next" , style: TextStyle(color: Colors.pink , fontSize: 20 , fontWeight: FontWeight.bold , fontStyle: FontStyle.italic),),
                color: Colors.amber,
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                elevation: 0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
