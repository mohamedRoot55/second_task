import 'package:flutter/material.dart';
import 'package:second_taxk/Splash/Splash2.dart';
import 'package:second_taxk/dataModels/UserLocation.dart';
import 'package:provider/provider.dart';
class Splash1 extends StatefulWidget {
  static const String routeName = "/Splash1";

  @override
  _Splash1State createState() => _Splash1State();
}

class _Splash1State extends State<Splash1> {
  @override
  Widget build(BuildContext context) {
    final ScreenHeigh = MediaQuery.of(context).size.height;
    Provider.of<UserLocation>(context);
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: ScreenHeigh,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Container(
                    width: 220,
                    height: 220,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/home1.png"))),
                    ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  "hello to our app",
                  style: TextStyle(
                    color: Colors.pink,
                    fontWeight: FontWeight.bold,
                    fontSize: 28,
                  ),
                ),
              ],
            ),
            Container(
              width: double.infinity,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed(Splash2.routeName);
                },
                child: Text(
                  "next",
                  style: TextStyle(
                      color: Colors.pink,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic),
                ),
                color: Colors.amber,
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                elevation: 0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
