import 'package:flutter/material.dart';

class Introduction extends StatelessWidget {
  static const String routeName = "/Introduction";

  @override
  Widget build(BuildContext context) {
    final ScreenHeigh = MediaQuery.of(context).size.height;
    final ScreenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                    height: ScreenHeigh * .2,
                    width: ScreenWidth * .4,
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.pink.withOpacity(.5),
                              blurRadius: 0.5,
                              offset: Offset(0.0, 0.0)),
                        ]),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(30),
                        child: Image.asset("assets/images/home1.png"))),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "it's your app",
                  style: TextStyle(
                      color: Colors.pink,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontStyle: FontStyle.italic),
                )
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(15),
                    height: ScreenHeigh * .2,
                    width: ScreenWidth * .4,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.orange.withOpacity(.5),
                              blurRadius: 0.5,
                              offset: Offset(0.0, 0.0)),
                        ]),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(30),
                        child: Image.asset("assets/images/location.png"))),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "it's easy to get your client's location ",
                  style: TextStyle(
                      color: Colors.pink,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontStyle: FontStyle.italic),
                )
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                    height: ScreenHeigh * .2,
                    width: ScreenWidth * .4,
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.redAccent.withOpacity(0.5),
                              blurRadius: 0.5,
                              offset: Offset(0.0, 0.0)),
                        ]),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(30),
                        child: Image.asset("assets/images/rocket1.png"))),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "fast and heigh performace",
                  style: TextStyle(
                      color: Colors.pink,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontStyle: FontStyle.italic),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
