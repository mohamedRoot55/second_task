import 'package:flutter/material.dart';
import 'package:second_taxk/Splash/Splash1.dart';
import 'package:second_taxk/Splash/Splash2.dart';
import 'package:second_taxk/Splash/Splash3.dart';
import 'package:second_taxk/home/home.dart';
import 'package:second_taxk/pages/Introduction.dart';
import 'package:provider/provider.dart';
import 'package:second_taxk/Services/locationServices.dart';
import 'package:second_taxk/dataModels/UserLocation.dart';

import 'dataModels/UserLocation.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<UserLocation>(
        create: (context) => LocationService().locationStream,
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: Splash1(),
          routes: {
            Home.routeName: (ctx) => Home(),
            Splash1.routeName: (ctx) => Splash1(),
            Splash2.routeName: (ctx) => Splash2(),
            Splash3.routeName: (ctx) => Splash3(),
            Introduction.routeName: (ctx) => Introduction(),
          },
        ));
  }

//  @override
//  Widget build(BuildContext context) {
//    return StreamBuilder<UserLocation>(
//      builder: (context) => LocationService().locationStream,
//      child: MaterialApp(
//        title: 'Flutter Demo',
//        theme: ThemeData(
//
//          primarySwatch: Colors.blue,
//        ),
//        home: Splash1(),
//        routes: {
//          Home.routeName : (ctx) => Home() ,
//          Splash1.routeName : (ctx) => Splash1() ,
//          Splash2.routeName : (ctx) => Splash2() ,
//          Splash3.routeName : (ctx) => Splash3() ,
//          Introduction.routeName : (ctx) => Introduction() ,
//
//
//        },
//      ),
//    );
//  }
}
