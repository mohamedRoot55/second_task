
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:second_taxk/Services/locationServices.dart';
import 'package:second_taxk/dataModels/UserLocation.dart';
import 'package:second_taxk/pages/HomeView.dart';
class Home extends StatefulWidget {
  static const String routeName = "/Home" ;
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return StreamProvider<UserLocation>(

      create: (context) => LocationService().locationStream,
      child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: Scaffold(
            body: HomeView(),
          )),
    );
  }

}
